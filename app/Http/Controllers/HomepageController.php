<?php

namespace App\Http\Controllers;
use App\Products;
use Illuminate\Http\Request;

class HomepageController extends Controller
{   
     public function index(){

    $products = Products::where('featured',true)->take(8)->inRandomOrder()->get();
    $hotproducts = Products::inRandomOrder()->take(4)->get();
    // dd($products);
    
    $data = array(
        "products"=> $products,
        "hotproducts"=> $hotproducts
    );
    // dd($data['products']);
    return view('home')->with($data);
    }
}
