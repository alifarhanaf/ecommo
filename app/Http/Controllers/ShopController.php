<?php

namespace App\Http\Controllers;
use App\Category;
use App\Products;
use Illuminate\Http\Request;

class ShopController extends Controller
{
    public function index(){

        if(request()->category){

                $products = Products::with('categories')->whereHas('categories',function($query){
                $query->where('slug',request()->category);
                

            });
            $categories = Category::all();
            $categoryName = $categories->where('slug',request()->category)->first()->name; 
            $hotproducts = Products::inRandomOrder()->take(4)->get();
           

        }else{

            $products = Products::where('featured',true);
            $hotproducts = Products::inRandomOrder()->take(4)->get();
            $categories = Category::all();
            $categoryName = 'Featured Items';

        }
        if(request()->sort == 'low_high'){
            $products = $products->orderBy('price')->paginate(7);

        }elseif(request()->sort == 'high_low'){
            $products = $products->orderBy('price','desc')->paginate(7);
        }else{
            $products = $products->paginate(7);
        }

        
        
        $data = array(
            "products"=> $products,
            "hotproducts"=> $hotproducts,
            "categories"=>$categories,
            "categoryName"=>$categoryName
        );
        // dd($data['products']);
        return view('shop')->with($data);
        }


    /**
     * Display the specified resource.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $products = Products::where('slug',$slug)->firstOrFail();
        $hotproducts = Products::inRandomOrder()->take(4)->get();
        $data = array(
            "product"=> $products,
            "hotproducts"=> $hotproducts
        );
        // view('includes.might-like')->with($data);
        // var_dump($data);
        // die;
        return view('singleproduct')->with($data);
       
    }
}
