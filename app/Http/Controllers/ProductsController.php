<?php

namespace App\Http\Controllers;
use App\Products;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    public function index()
    {
       
    }
    public function products()
    {
        $products = Products::inRandomOrder()->take(8)->get();
        $hotproducts = Products::inRandomOrder()->take(4)->get();
        
        $data = array(
            "products"=> $products,
            "hotproducts"=> $hotproducts
        );
        // dd($data['products']);
        return view('products')->with($data);
    }
    public function productdetail()
    {
        $products = Products::inRandomOrder()->take(8)->get();
        $hotproducts = Products::inRandomOrder()->take(4)->get();
        
        $data = array(
            "products"=> $products,
            "hotproducts"=> $hotproducts
        );
        // dd($data['products']);
        return view('singleproduct')->with($data);
    }
    public function cart()
    {
        $products = Products::inRandomOrder()->take(8)->get();
        $hotproducts = Products::inRandomOrder()->take(4)->get();
        
        $data = array(
            "products"=> $products,
            "hotproducts"=> $hotproducts
        );
        // dd($data['products']);
        return view('cart')->with($data);
    }
    public function checkout()
    {
        $products = Products::inRandomOrder()->take(8)->get();
        $hotproducts = Products::inRandomOrder()->take(4)->get();
        
        $data = array(
            "products"=> $products,
            "hotproducts"=> $hotproducts
        );
        // dd($data['products']);
        return view('checkout')->with($data);
    }
    public function thanks()
    {
        $products = Products::inRandomOrder()->take(8)->get();
        $hotproducts = Products::inRandomOrder()->take(4)->get();
        
        $data = array(
            "products"=> $products,
            "hotproducts"=> $hotproducts
        );
        // dd($data['products']);
        return view('thanks')->with($data);
    }
}
