<?php

namespace App\Http\Controllers;

use App\Products;
use Illuminate\Http\Request;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\Validator;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hotproducts = Products::inRandomOrder()->take(4)->get();
        $data = array(
            "hotproducts"=> $hotproducts
        );
        return view('cart')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $hello = $request->id;
        $duplicates = Cart::search(function ($cartItem, $rowId) use($request) {
            return $cartItem->id ===  $request->id ;
        });
        // {{dd($duplicates);}}
         if($duplicates->isNotEmpty()){
            return redirect()->route('cart.index')->with('success_message','Items already in Cart');
         }
        
            
       
        Cart::add($request->id,$request->name,1,$request->price)
        ->associate('App\Products');

        return redirect()->route('cart.index')->with('success_message','Item has been added to Cart');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),[
            'quantity'=>'required|numeric|between:1,5'
        ]);

        if($validator->fails()){
            session()->flash('errors',collect(['Quantity Must be between 1 to 5']));
            return response()->json(['success'=> false], 400);
        }
        Cart::update($id,$request->quantity);
        session()->flash('success_message','Quantity Updated Successfully');
        return response()->json(['success'=> true]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Cart::remove($id); 
        return back()->with('success_message','Item Removed Successfully');
    }
    /**
     * To save the item For Later .
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function switchToSaveForLater($id)
    {
        $item = Cart::get($id);
        Cart::remove($id); 

        $duplicates = Cart::instance('saveForLater')->search(function ($cartItem, $rowId) use($id) {
            return $rowId ===  $id ;
        });
        // {{dd($duplicates);}}
         if($duplicates->isNotEmpty()){
            return redirect()->route('cart.index')->with('success_message','Item is already Saved For Later');
         }
         
        Cart::instance('saveForLater')->add($item->id,$item->name,1,$item->price)
        ->associate('App\Products');

        return redirect()->route('cart.index')->with('success_message','Item has been saved for Later');
    }
}
