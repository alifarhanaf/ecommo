<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    public function categories(){
        return $this->belongsToMany('App\Category');
    }

    public function presentPrice(){
        $price = number_format($this->price, 2); 
        return "$"." ".$price;
    }
   
}
