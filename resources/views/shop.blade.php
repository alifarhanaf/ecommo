@extends('layout')

@section('title', 'Products')

@section('extra-css')

@endsection

@section('content')
<div class="container">
    <div class="breadcrumbs">
       
            <span class="sidemargin">Home</span>
            <i class="fa fa-chevron-right breadcrumb-separator"></i>
            <span>Shop</span>
        </div> <!-- end breadcrumbs -->
    </div> 
    <div class=" container">
    
    <div class="products-section">
        <div class="sidebar">
            <h3>By Category</h3>
            <ul class="no-bullets">
                @foreach ($categories as $category)
                {{-- {{request()->category}}
                {{$category->slug}} --}}
            <li class="{{ (request()->category == $category->slug ? 'active' : 'not-active') }}">
                <a href="{{ route('shop.index',['category'=>$category->slug])}}">{{$category->name}}</a>
            </li>    
                 
                @endforeach
            </ul>
                
            

            
        </div> <!-- end sidebar -->
        <div>
        {{-- <h1 class="stylish-heading">{{$categoryName}}</h1> --}}
        <div class="products-header">
            <h1 class="stylish-heading">{{ $categoryName }}</h1>
            <div>
                <strong>Price:</strong>
                <a href="{{ route('shop.index', ['category'=> request()->category, 'sort' => 'low_high']) }}">Low to High</a> |
                <a href="{{ route('shop.index', ['category'=> request()->category, 'sort' => 'high_low']) }}">High to Low</a>
            </div>
        </div>
        <div class="products text-center">

            @forelse ($products as $product)
            <div class="product">
                <a href="{{ route('shop.show',$product->slug) }}"><img src="{{ asset('storage/imgs/products/'.$product->slug.'.png')}}" alt="product" class="products-image"></a>
                    <a href="{{ route('shop.show',$product->slug) }}"><div class="product-name">{{$product->name}}</div></a>
                    <div class="product-price">{{$product->presentPrice()}}</div>
                </div>    
                
            @empty
                <div style="text-align:left"> No Items Found</div>
            @endforelse


            @foreach ($products as $product)
            
            @endforeach
            
            
            </div> <!-- end products -->
            <div class="spacer"></div> 
           
           <div class="text-center">
            {{$products->appends(request()->input())->links()}}
           </div>
        </div>
       
    </div>
</div>
    


@endsection