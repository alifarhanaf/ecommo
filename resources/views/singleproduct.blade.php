@extends('layout')

@section('title', $product->name)

@section('extra-css')

@endsection

@section('content')
<div class="container">
    <div class="breadcrumbs">
        
            <span class="sidemargin">Home</span>
            <i class="fa fa-chevron-right breadcrumb-separator"></i>
            <span>Shop</span>
            <i class="fa fa-chevron-right breadcrumb-separator"></i>
            <span> {{$product->name}}</span>
        </div>
    </div> <!-- end breadcrumbs -->
    <div class="container">
    <div class="product-section">
        <div class="product-section-image">
            <img src="{{ asset('storage/imgs/products/'.$product->slug.'.png')}}" alt="product" class="single-product-image" >
        </div>
        <div class="product-section-information">
            <h1 class="product-section-title">{{$product->name}}</h1>
        <div class="product-section-subtitle">{{$product->details}}</div>
            <div class="product-section-price">{{$product->presentPrice()}}</div>


            <p>
                {{$product->description}}
            </p>
           <p>&nbsp</p> 
        <form action="{{ route('cart.store') }}" method="POST" >
        {{ csrf_field() }}
        <input type="hidden" name="id" value="{{ $product->id }}">
        <input type="hidden" name="name" value="{{ $product->name }}">
        <input type="hidden" name="price" value="{{ $product->price }}">
        <button type="submit" class="button button-plain"> Add to Cart </button>
        </form>
            {{-- <a class="button "> Add to Cart </a>       --}}
        </div>
    </div> <!-- end product-section -->
</div>
    {{-- @foreach ($hotproducts as $product)
        {{$product->name}}
    @endforeach --}}
    {{-- {{$mightLikeProducts}} --}}
    @include('includes.might-like')

@endsection