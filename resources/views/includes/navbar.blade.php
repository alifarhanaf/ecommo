<header>
<div class="  container">
    <div class=" top-nav">
    <div class="logo"><a href="{{ route('homepage')}}"> Tee Spring</a></div>
    <ul>
    <a href="{{ route('shop.index') }}">Shop</a>
        <a href="#">About</a>
        <a href="#">Blog</a>
        {{-- <a href="#">Cart</a> --}}
        <a href="{{ route('cart.index') }}">Cart <span class="cart-count">
            @if(Cart::instance('default')->count()>0)
                <span>{{ Cart::instance('default')->count()}}</span></span>
            @endif
        </a>
    </ul>
</div>
</div> <!-- End Top Nav -->
<hr>
<div class="container">
<div class="sub-nav">
    <ul>
        <a href="#">Mens</a>
        <a href="#">Women</a>
        <a href="#">Kids</a>
        <a href="#">Teeagers</a>
    </ul>
    
</div> <!-- End Sub Nav -->
</div>
</header>
