<div class="container">
    <div class="might-like-section">
        
            <h2>You might also like...</h2>

            <div class="might-like-grid">
               
               
                    @foreach ($hotproducts as $product)
                    <div class="might-like-product">
                    <a href="{{ route('shop.show',$product->slug) }}"><img src="{{ asset('storage/imgs/products/'.$product->slug.'.png')}}" alt="product" class="ml-img"></a>
                    <br>
                    <a href="{{ route('shop.show',$product->slug) }}" class="might-like-product-name">{{$product->name}}</a>
                    <br>
                    <a class="might-like-product-price">{{$product->presentPrice()}}</a>
                    </div>
                    @endforeach
                
               
                {{-- <div class="might-like-product">
                    <img src="{{ asset('/storage/imgs/noimage.jpeg') }}" alt="product">
                    <br>
                    <a class="might-like-product-name">MacBook Pro</a>
                    <br>
                    <a class="might-like-product-price">$2499.99</a>
                </div>
                <div class="might-like-product">
                    <img src="{{ asset('/storage/imgs/noimage.jpeg') }}" alt="product">
                    <br>
                    <a class="might-like-product-name">MacBook Pro</a>
                    <br>
                    <a class="might-like-product-price">$2499.99</a>
                </div>
                <div class="might-like-product">
                    <img src="{{ asset('/storage/imgs/noimage.jpeg') }}" alt="product">
                    <br>
                    <a class="might-like-product-name">MacBook Pro</a>
                    <br>
                    <a class="might-like-product-price">$2499.99</a>
                </div>
                <div class="might-like-product">
                    <img src="{{ asset('/storage/imgs/noimage.jpeg') }}" alt="product">
                    <br>
                    <a class="might-like-product-name">MacBook Pro</a>
                    <br>
                    <a class="might-like-product-price">$2499.99</a>
                </div> --}}
            </div>
        </div>
    </div>
