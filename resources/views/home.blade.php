<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <title>{{ config('app.name', 'TeeSpring') }}</title>

    <!-- Scripts -->
    {{-- <link rel="stylesheet" href="{{asset('css/app.css')}}"> --}}
    {{-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" 
    integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" 
    integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script> --}}
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" 
    integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    {{-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" 
    integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script> --}}
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat|Roboto:300,400,700" rel="stylesheet">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ mix('css/responsive.css') }}">

</head>
<body>
    <header>
        <div class="container">
        <div class=" top-nav ">
        <div class="logo"> <a href="{{ route('homepage')}}">Tee Spring</a></div>
            <ul>
                <a href="{{ route('shop.index') }}">Shop</a>
                <a href="#">About</a>
                <a href="#">Blog</a>
                <a href="{{ route('cart.index') }}">Cart <span class="cart-count">
                    @if(Cart::instance('default')->count()>0)
                        <span>{{ Cart::instance('default')->count()}}</span></span>
                    @endif
                </a>
            </ul>
            
        </div> <!-- End Top Nav -->
        <hr>
        

        
        <div class="sub-nav ">
            <ul>
                <a href="#">Mens</a>
                <a href="#">Women</a>
                <a href="#">Kids</a>
                <a href="#">Teeagers</a>
            </ul>
            
        </div> <!-- End Sub Nav -->
        
        
        <div class="hero ">
            <div class="hero-copy text-center">
                <h1>Find Something You Love</h1>
                <!-- <div class="search">
                    <input type="search" class="search-box" />
                    <span class="search-button">
                      <span class="search-icon"></span>
                    </span>
                  </div> -->
                  <div class="wp-wrapper">
                  <div class="wrapper">
                    <input type="text" class="input" 
                    placeholder="What are you looking for?">
                    <div class="searchbtn"><i class="fa fa-search"></i></div>
                </div>
                  </div>
                <div class="hero-buttons text-center">
                <a class=" button">GitHub Link</a>
                
                </div>
           </div> <!--End Hero Copy-->
           <div class="hero-image">
               <img src="/storage/imgs/hh.svg" alt="hero image">
           </div>
        </div><!--End Hero Container-->
        <div class="basehero ">
            {{-- <div class="row mt-3">
                <div class="col-md-8"> --}}
            <p class="basetext text-center"><b>Lorem Ipsum is simply dummy text ?</b></p>
           <div class="join text-center">
            <a class="  button  ">Join US</a>
           </div>
                 
                 
        </div>
        </div>
    </header>

    <div class="featured-section">
        <div class="container">
            <h1 class="mt-4"> Recommended For You</h1>
            <p class="section-description mb-4" style="text-align: left">
                Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                

            </p>
            <!-- <div class="text-center button-container">
                <a class="btn btn-secondary">Button 1</a>
                <a class="btn btn-secondary">Button 1</a>
            </div> -->
         
        <div class="products text-center">
            @foreach ($products as $product)
            <div class="product">
                <a href="{{ route('shop.show',$product->slug) }}"><img src="{{ asset('storage/imgs/products/'.$product->slug.'.png')}}" alt="product" class="home-img"></a>
                <br>
                <a href="{{ route('shop.show',$product->slug) }}"><span class="product-name">{{$product->name}}</span></a>
                
                <div class="product-price text-center"><b>{{$product->presentPrice()}}</b></div>
            </div>
                
            @endforeach
            
            
        </div> <!-- End Products -->


    </div><!-- End Container -->

</div> <!-- End Featured Section-->


<div class="hotfav-section">
    <div class="container">
        <h1 class="mt-4"> Weekly Hot Favourite</h1>
        
    <div class="products text-center">
        @foreach ($hotproducts as $product)
        <div class="product">
        <a href="{{ route('shop.show',$product->slug) }}"><img src="{{ asset('storage/imgs/products/'.$product->slug.'.png')}}" alt="product" class="home-img"></a>
            <br>
            <a href="{{ route('shop.show',$product->slug) }}"><span class="product-name">{{$product->name}}</span></a>
            <div class="product-price text-center"><b>{{$product->presentPrice()}}</b></div>
        </div>
            
        @endforeach
       
    </div> <!-- End Products -->


</div><!-- End Container -->

</div> <!-- End Featured Section-->
    <div class="container">
        <div class="blog-section ">
    
        <h1 class="text-center"> From Bloggers</h1>
        <p class="section-description-blog">
            Lorem Ipsum is simply dummy text of the printing and typesetting industry.
            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
            when an unknown printer took a galley of type and scrambled it too.
           

        </p>
        <div class="blog-posts ">
            <div class="blog-post">
                <a href="#"><img src="/storage/imgs/noimage.jpeg" alt="blog image"></a>
                <a href="#"><h2 class="blog-title mt-4">Blog Lumber One</h2></a>
                <div class="blog-description">Lorem Ipsum is simply dummy text of the printing and typesetting indo.</div>
            </div>
            <div class="blog-post">
                <a href="#"><img src="/storage/imgs/noimage.jpeg" alt="blog image"></a>
                <a href="#"><h2 class="blog-title mt-4">Blog Lumber One</h2></a>
                <div class="blog-description">Lorem Ipsum is simply dummy text of the printing and typesetting indo.</div>
            </div>
            <div class="blog-post">
                <a href="#"><img src="/storage/imgs/noimage.jpeg" alt="blog image"></a>
                <a href="#"><h2 class="blog-title mt-4">Blog Lumber One</h2></a>
                <div class="blog-description">Lorem Ipsum is simply dummy text of the printing and typesetting indo.</div>
            </div>
        </div><!-- End Blog Posts-->

    </div> <!-- End Cotainer -->
</div> <!--End Blog Section -->
<div class="container">
<footer>
    <div class="footer-content ">
            <ul>
                <a href="#"><i class="fa fa-globe"></i></a>
                <a href="#"><i class="fa fa-youtube"></i></a>
                <a href="#"><i class="fa fa-github"></i></a>
                <a href="#"><i class="fa fa-twitter"></i></a>
            </ul>
            <div class="made-with" ><a> Made With <i class="fa fa-heart"></i> By Farhan Ali</a></div>
    </div> <!--End Footer Container--> 
</footer>
</div>
</body>
</html>
