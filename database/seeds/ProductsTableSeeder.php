<?php
use App\Products;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 10; $i++) {
            Products::create([
                'name' => 'TShirt ' . $i,
                'slug' => 'tshirt-' . $i,
                'details' => ['Crew Neck', 'Full Sleeve', 'V-Neck','Rounded Neck'][array_rand(['Crew Neck', 'Full Sleeve', 'V-Neck','Rounded Neck'])] ,
                'price' => rand(1000, 2000),
                'description' => 'Lorem ' . $i . ' ipsum dolor sit amet, consectetur adipisicing elit. Ipsum temporibus iusto ipsa, asperiores voluptas unde aspernatur praesentium in? Aliquam, dolore!',
            ])->categories()->attach(1);
        }
        for ($i = 1; $i <= 10; $i++) {
            Products::create([
                'name' => 'Formal Shirts ' . $i,
                'slug' => 'formalshirt-' . $i,
                'details' => ['Smart Collar', 'Traditional Collar', 'Formal Collar'][array_rand(['Smart Collar', 'Traditional Collar', 'Formal Collar'])] . ' , ' . ['Cotton Fabric', 'Twill Fabric', 'Linen Fabric'][array_rand(['Cotton Fabric', 'Twill Fabric', 'Linen Fabric'])] ,
                'price' => rand(2000, 4000),
                'description' => 'Lorem ' . $i . ' ipsum dolor sit amet, consectetur adipisicing elit. Ipsum temporibus iusto ipsa, asperiores voluptas unde aspernatur praesentium in? Aliquam, dolore!',
            ])->categories()->attach(2);
        }
        for ($i = 1; $i <= 10; $i++) {
            Products::create([
                'name' => 'Dress Shirts ' . $i,
                'slug' => 'dressshirt-' . $i,
                'details' => ['Double Cough','Single Cough'][array_rand(['Double Cough','Single Cough'])] . ', ' . ['Plain', 'Striped', 'Checked'][array_rand(['Plain', 'Striped', 'Checked'])] ,
                'price' => rand(3000, 5000),
                'description' => 'Lorem ' . $i . ' ipsum dolor sit amet, consectetur adipisicing elit. Ipsum temporibus iusto ipsa, asperiores voluptas unde aspernatur praesentium in? Aliquam, dolore!',
            ])->categories()->attach(3);
        }
        for ($i = 1; $i <= 10; $i++) {
            Products::create([
                'name' => 'Jeans ' . $i,
                'slug' => 'jeans-' . $i,
                'details' => ['Formal Trousers','Denim Jeans','Mens Chinos'][array_rand(['Formal Trousers','Denim Jeans','Mens Chinos'])] . ', ' . ['Skin Fit', 'Slim Fit', 'Narrow'][array_rand(['Skin Fit', 'Slim Fit', 'Narrow'])] ,
                'price' => rand(2000, 5000),
                'description' => 'Lorem ' . $i . ' ipsum dolor sit amet, consectetur adipisicing elit. Ipsum temporibus iusto ipsa, asperiores voluptas unde aspernatur praesentium in? Aliquam, dolore!',
            ])->categories()->attach(4);
        }
        for ($i = 1; $i <= 10; $i++) {
            Products::create([
                'name' => 'Dress Pants ' . $i,
                'slug' => 'dresspant-' . $i,
                'details' => ['Classic Fit','Smart Fit'][array_rand(['Classic Fit','Smart Fit'])] . ', ' . ['Plain', 'Striped', 'Checked'][array_rand(['Plain', 'Striped', 'Checked'])] ,
                'price' => rand(2000, 5000),
                'description' => 'Lorem ' . $i . ' ipsum dolor sit amet, consectetur adipisicing elit. Ipsum temporibus iusto ipsa, asperiores voluptas unde aspernatur praesentium in? Aliquam, dolore!',
            ])->categories()->attach(5);
        }
        for ($i = 1; $i <= 10; $i++) {
            Products::create([
                'name' => 'Accessories ' . $i,
                'slug' => 'accessories-' . $i,
                'details' => ['Watch','Belt'][array_rand(['Watch','Belt'])] . ', ' . ['Basic Accessory', 'In-Trend'][array_rand(['Basic Accessory', 'In-Trend'])] ,
                'price' => rand(500, 1200),
                'description' => 'Lorem ' . $i . ' ipsum dolor sit amet, consectetur adipisicing elit. Ipsum temporibus iusto ipsa, asperiores voluptas unde aspernatur praesentium in? Aliquam, dolore!',
            ])->categories()->attach(6);
        }
        for ($i = 1; $i <= 10; $i++) {
            Products::create([
                'name' => 'Shoes ' . $i,
                'slug' => 'shoe-' . $i,
                'details' => ['Sneaker','Formal','Loafers','Sandals'][array_rand(['Sneaker','Formal','Loafers','Sandals'])] . ', ' . ['Basic', 'In-Trend'][array_rand(['Basic', 'In-Trend'])] ,
                'price' => rand(2000, 4000),
                'description' => 'Lorem ' . $i . ' ipsum dolor sit amet, consectetur adipisicing elit. Ipsum temporibus iusto ipsa, asperiores voluptas unde aspernatur praesentium in? Aliquam, dolore!',
            ])->categories()->attach(7);
        }
        
        // Products::create([
        //     'name'=> 'T Shirt 10',
        //     'slug'=> 'tshirt-10',
        //     'details'=> 'Full Sleeve T Shirt',
        //     'price'=> '2000',
        //     'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
        //     Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley'
        // ]);
    }
}
