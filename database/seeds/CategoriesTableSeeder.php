<?php

use App\Category;
use Illuminate\Support\Carbon;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now()->toDateTimeString();

        Category::insert([
            ['name'=>'T Shirts','slug'=>'tshirts','created_at'=>$now,'updated_at'=>$now],
            ['name'=>'Formal Shirts','slug'=>'formalshirts','created_at'=>$now,'updated_at'=>$now],
            ['name'=>'Dress Shirts','slug'=>'dressshirts','created_at'=>$now,'updated_at'=>$now],
            ['name'=>'Jeans','slug'=>'jeans','created_at'=>$now,'updated_at'=>$now],
            ['name'=>'Dress Pants','slug'=>'dresspants','created_at'=>$now,'updated_at'=>$now],
            ['name'=>'Accessories','slug'=>'accessories','created_at'=>$now,'updated_at'=>$now],
            ['name'=>'Shoes','slug'=>'shoes','created_at'=>$now,'updated_at'=>$now]
        ]);
    }
}
